import sys
#from pyairports.airports import Airports
#from find_airport import find_ap
import sqlite3

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


nextFlightId = 0
flightsList = list()
flightMap = dict()
apMap = dict()

def prepateAirports():
    global apMap



    conn = sqlite3.connect('airport_codes.db')
    cur = conn.cursor()
    cur.execute('''SELECT iata_code, iso_country, iso_region, municipality FROM airport_codes''')
    for row in cur.fetchall():
        if row[0] is not None:
            apMap[row[0]] = row[1:]
    conn.close()

    conn = sqlite3.connect('merged.db')
    cur = conn.cursor()
    cur.execute('''SELECT distinct Departure_Airport, Departure_City FROM Privilege_Registered_Flights 
            UNION SELECT distinct Arrival_Airport, arrival_City FROM Privilege_Registered_Flights''')
    # print(cur.fetchall())
    for row in cur.fetchall():
        newrow = [None, None, row[1]]
        if row[0] is not None:
            apMap[row[0]] = newrow = [None, None, row[1]]
    conn.close()

def apToCity(ap):
    row = apMap[ap]
    city = row[2]
    # city = row[2].replace(' ', '') if '/' in row[2] else row[2]
    # city.replace('Saint ', 'St. ')
    if row[0] == 'US':
        return city + ' ' + row[1].split('-')[1]
    else:
        return city



def flights_insert(cur,Flight_code,Departure_City, Arrival_City, Date):
    global nextFlightId, flightsList, flightMap
    found = False
    result_id = None
    Departure_City = Departure_City.upper() if Departure_City is not None else None
    Arrival_City = Arrival_City.upper() if Arrival_City is not None else None
    # airports = Airports()
    try:
        if len(Departure_City) == 3:
            Departure_City = apToCity(Departure_City)
        if len(Arrival_City) == 3:
            Arrival_City = apToCity(Arrival_City)
    except Exception:
        print('Airport not found in pyairport ' + str([Flight_code, Departure_City, Arrival_City, Date]))

    Departure_City = Departure_City.upper() if Departure_City is not None else None
    Arrival_City = Arrival_City.upper() if Arrival_City is not None else None


    if Flight_code in flightMap:
        if Date in flightMap[Flight_code]:
            for flight in flightMap[Flight_code][Date]:
                if (flight[2] == Departure_City or Departure_City is None or flight[2] is None) and (flight[3] == Arrival_City or Arrival_City is None or flight[3] is None):
                    if flight[2] is None:
                        flight[2] = Departure_City
                    if flight[3] is None:
                        flight[3] = Arrival_Airport
                    found = True
                    result_id = flight[0]
                    break
                elif flight[3] != Departure_City and flight[2] != Arrival_City and (flight[2] != Departure_City or flight[3] != Arrival_City):
                        print('Collision %s to %s' % ((Flight_code, Departure_City, Arrival_City, Date), flight))

                if not found:
                    flightMap[Flight_code][Date].append([nextFlightId,Flight_code, Departure_City, Arrival_City, Date])
                    result_id = nextFlightId
                    nextFlightId += 1
                    found = True

        else:
            l = [[nextFlightId,Flight_code, Departure_City, Arrival_City, Date]]
            flightMap[Flight_code][Date] = l
            result_id = nextFlightId
            nextFlightId += 1
            found = True
            

    # for flight in flightsList:
    #     if flight[1] == Flight_code and flight[2] == Departure_City and flight[3] == Arrival_City and flight[4] == Date:
    #         found = True
    #         result_id = flight[0]
    #         eprint('Collision %s %s %s %s %s %s' % (Flight_code, Departure_City, Arrival_City, Date, result_id, nextFlightId))
    #         break


    if not found:
        l = [[nextFlightId,Flight_code, Departure_City, Arrival_City, Date]]
        d = {Date:l}
        flightMap[Flight_code] = d
        result_id = nextFlightId
        nextFlightId += 1

    return result_id


def insertFlightsToDb(cursor):
    global flightMap
    targetList = []
    for m in list(flightMap.values()):
        for l in list(m.values()):
            for t in l:
                targetList.append(t)
    # noinspection SqlNoDataSourceInspection
    cursor.execute('''DROP TABLE IF EXISTS Flights''')
    # noinspection SqlNoDataSourceInspection
    cursor.execute('''CREATE TABLE Flights (flightId int PRIMARY KEY, Flight_code text, Arrival_City text, Departure_City text, 
                                        Date text) ''')
    # noinspection SqlNoDataSourceInspection
    cursor.executemany('''INSERT INTO Flights VALUES(?,?,?,?,?)''', targetList)
