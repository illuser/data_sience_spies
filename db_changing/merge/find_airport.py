import sys
import sqlite3


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def find_ap(cur, ap):
    # noinspection SqlNoDataSourceInspection
    cur.execute('''SELECT iso_country, iso_region, municipality FROM airport_codes WHERE iata_code = "%s"''' % ap)
    rows = cur.fetchall()
    if len(rows) != 1:
        eprint('strange response: %s for %s' % (rows, ap))
        return None
    elif rows[0][0] == 'US':
        return rows[0][2] + ' ' + rows[0][1].split('-')[1]
    else:
        return rows[0][2]


if __name__ == '__main__':
    conn = sqlite3.connect('airport_codes.db')

    cur = conn.cursor()
    print(find_ap(cur, 'ATL'))
    print(find_ap(cur, 'SVO'))
