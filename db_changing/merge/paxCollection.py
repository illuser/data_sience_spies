#! data structure:
#   integer id, string firstname, string secondname, string lastname, None/string passport, None/[string card]
# 0 ~ id
# 1 ~ first name
# 2 ~ second name
# 3 ~ last name
# 4 ~ passport
# 5 ~ cards
import sys
import sqlite3

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


nextPaxId = 0
paxList = list()
paxPassportMap = {}
paxCardMap = {}
paxCollisionMap = {}
paxFixMap = {}


def find_or_insert(first_name, second_name, last_name, passport, card):
    global nextPaxId, paxList, paxPassportMap, paxCardMap
    _passport = passport.replace(' ', '') if passport is not None else None
    _card = card.replace(' ', '') if card is not None else None
    found = False
    result_id = None
    if _passport in paxPassportMap:
        pax = paxPassportMap[_passport]
        result_id = pax[0]
        found = True
        found_here = True
        if _card is not None and _card not in pax[5]:
            if _card not in paxCardMap:
                paxCardMap[_card] = pax
            else:
                eprint('Card collision %s to %s/%s' % ((first_name, second_name, last_name, _passport, _card), paxCardMap[_card], pax))
                if paxCardMap[_card][4] is not None and paxCardMap[_card][1] == pax[1] and paxCardMap[_card][3] == pax[3]:
                    if pax[4] not in paxCollisionMap:
                        if paxCardMap[_card][4] in paxCollisionMap:
                            paxCollisionMap[pax[4]] = paxCollisionMap[paxCardMap[_card][4]]
                        else:
                            paxCollisionMap[pax[4]] = pax[4]
                            paxCollisionMap[paxCardMap[_card][4]] = pax[4]
                    if paxCardMap[_card][4] not in paxCollisionMap:
                        paxCollisionMap[paxCardMap[_card][4]] = paxCollisionMap[pax[4]]
                    


                        
            pax[5].append(_card)

        if first_name is not None:
                if pax[1] is None:
                    pax[1] = first_name
                elif pax[1] != first_name:
                    eprint(
                        'First name collision %s to %s' % (
                            (first_name, second_name, last_name, _passport, _card), pax))
        if last_name is not None:
            if pax[3] is None:
                pax[3] = last_name
            elif pax[3] != last_name:
                eprint(
                    'Last name collision %s to %s' % (
                        (first_name, second_name, last_name, _passport, _card), pax))
        if second_name is not None:
            if pax[2] is None:
                pax[2] = second_name
            else:
                if pax[2][0] != second_name[0]:
                    print(
                        'Possible second name collision %s to %s' % (
                            (first_name, second_name, last_name, _passport, _card), pax))
                elif len(pax[2]) < len(second_name):
                    pax[2] = second_name

    if _card in paxCardMap:
        pax = paxCardMap[_card]
        if not found:
            result_id = pax[0]
            found = True
            found_here = True
        elif pax[0] != result_id:
            eprint('Collision %s to %s' % ((first_name, second_name, last_name, _passport, _card), pax))
        if _passport is not None:
            if pax[4] is None:
                pax[4] = _passport
                if _passport not in paxPassportMap:
                    paxPassportMap[_passport] = pax
                else:
                    eprint(
                    'Passport map collision %s to %s/%s' % (
                        (first_name, second_name, last_name, _passport, _card), paxPassportMap[_passport], pax))
                    if paxPassportMap[_passport][1] == pax[1] and paxPassportMap[_passport][3] == pax[3]:
                        if pax[4] not in paxCollisionMap:
                            if paxPassportMap[_passport][4] in paxCollisionMap:
                                paxCollisionMap[pax[4]] = paxCollisionMap[paxPassportMap[_passport][4]]
                            else:
                                paxCollisionMap[pax[4]] = pax[4]
                                paxCollisionMap[paxPassportMap[_passport][4]] = pax[4]
                        if paxPassportMap[_passport][4] not in paxCollisionMap:
                            paxCollisionMap[paxPassportMap[_passport][4]] = paxCollisionMap[pax[4]]
            elif pax[4] != _passport:
                eprint(
                    'Passport collision %s to %s' % (
                        (first_name, second_name, last_name, _passport, _card), pax))
                if first_name == pax[1] and last_name == pax[3]:
                    if pax[4] not in paxCollisionMap:
                        if _passport in paxCollisionMap:
                            paxCollisionMap[pax[4]] = paxCollisionMap[_passport]
                        else:
                            paxCollisionMap[pax[4]] = pax[4]
                            paxCollisionMap[_passport] = pax[4]
                    if _passport not in paxCollisionMap:
                        paxCollisionMap[_passport] = paxCollisionMap[pax[4]]

        if first_name is not None:
                if pax[1] is None:
                    pax[1] = first_name
                elif pax[1] != first_name:
                    eprint(
                        'First name collision %s to %s' % (
                            (first_name, second_name, last_name, _passport, _card), pax))
        if last_name is not None:
            if pax[3] is None:
                pax[3] = last_name
            elif pax[3] != last_name:
                eprint(
                    'Last name collision %s to %s' % (
                        (first_name, second_name, last_name, _passport, _card), pax))
        if second_name is not None:
            if pax[2] is None:
                pax[2] = second_name
            else:
                if pax[2][0] != second_name[0]:
                    print(
                        'Possible second name collision %s to %s' % (
                            (first_name, second_name, last_name, _passport, _card), pax))
                elif len(pax[2]) < len(second_name):
                    pax[2] = second_name

    if not found:
        paxList.append([nextPaxId, first_name, second_name, last_name, _passport, [_card] if _card is not None else []])
        if (_passport is not None):
            paxPassportMap[_passport] = paxList[-1]
        if (_card is not None):
            paxCardMap[_card] = paxList[-1]            
        result_id = nextPaxId
        nextPaxId += 1
    return result_id


def insertToDb(cursor):

    global paxCollisionMap, paxFirstName, paxList, paxPassportMap

    for key in paxCollisionMap:
        substitution = paxCollisionMap[key]
        if substitution != key:
            try:
                for card in paxPassportMap[key][5]:
                    if card not in paxPassportMap[substitution][5]:
                        paxPassportMap[substitution][5].append(card)
                paxFixMap[paxPassportMap[key][0]] = paxPassportMap[substitution][0]
                paxPassportMap[key][0] = -1
                # print('FIXED %s <- %s' % (paxPassportMap[substitution], paxPassportMap[key]))
            except:
                pass

    insList = []

    # noinspection SqlNoDataSourceInspection
    cursor.execute('''DROP TABLE IF EXISTS Pax''')
    # noinspection SqlNoDataSourceInspection
    cursor.execute('''CREATE TABLE Pax (paxId int PRIMARY KEY, paxFirstName text, paxSecondName text, paxLastName text, 
                                        passport text, cards text) ''')
    for pax in paxList:
        if pax[0] == -1:
            continue
        pax5 = None
        if type(pax[5]) is list:
            pax5 = ';'.join(pax[5])
            if pax5 == '':
                pax5 = None
        insList.append(pax[0:5] + [pax5])

    # print(insList[-1])
    # noinspection SqlNoDataSourceInspection
    cursor.executemany('''INSERT INTO Pax VALUES(?,?,?,?,?,?)''', insList)
    return paxFixMap


if __name__ == '__main__':
    inserts = []
    inserts.append(find_or_insert('IVAN', 'I', 'IVANOV', '400', '1'))
    inserts.append(find_or_insert('IVAN', 'J', 'IVANOV', '400', '1'))
    assert inserts[0] == inserts[1]
    inserts.append(find_or_insert('IVAN', 'IVANOVICH', 'IVANOV', '400', '123'))
    assert inserts[0] == inserts[2]
    assert len(paxList[0][5]) > 1
    assert '123' in paxList[0][5]
    assert paxList[0][2] == 'IVANOVICH'
    inserts.append(find_or_insert('IVAN', 'J', 'IVANOV', None, '3'))
    assert len(paxList) == 2
    inserts.append(find_or_insert('IVAN', 'JOSEPH', 'IVANOV', '401', '3'))
    inserts.append(find_or_insert('IVAN', 'JOSEF', 'IVANOV', '401', '3'))
    inserts.append(find_or_insert('JVAN', 'JOSEPH', 'IVANOV', '401', '3'))
    inserts.append(find_or_insert('IVAN', 'JOSEPH', 'IVANOFF', '401', '3'))
    inserts.append(find_or_insert('IVAN', None, 'IVANOV', '400', '3'))
    inserts.append(find_or_insert('IVAN', None, 'IVANOV', '401', '1'))
    print(paxList)
    conn = sqlite3.connect('/tmp/paxCollectionTest.db')
    c = conn.cursor()
    insertToDb(c)
    conn.commit()
    conn.close()
    print('Success')
