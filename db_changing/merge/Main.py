
import sqlite3
from db__select import selectFromBD
from flightCollection import flights_insert
from flightCollection import insertFlightsToDb
from flightCollection import prepateAirports
from paxCollection import find_or_insert
from paxCollection import insertToDb

def insertPaxFlights(cursor,toInsert):
    # noinspection SqlNoDataSourceInspection
    cursor.execute('''DROP TABLE IF EXISTS paxflight''')
    # noinspection SqlNoDataSourceInspection
    cursor.execute('''CREATE TABLE paxflight (paxId int, flightId int, Fare text, 
    FOREIGN KEY(paxId) REFERENCES Pax(paxId),
    FOREIGN KEY(flightId) REFERENCES Flights(flightId),
    PRIMARY KEY(paxId,flightId)) ''')
    # noinspection SqlNoDataSourceInspection
    for i in toInsert:
        try:
            cursor.execute('INSERT INTO paxflight VALUES(?,?,?) ', i)
        except Exception as e:
            pass
            #print(i)
            #print(e)


prepateAirports()

conn = sqlite3.connect('merged.db')
cur = conn.cursor()
tableInfo = selectFromBD(cur)
toInsert = []
for row in tableInfo:
    paxId = find_or_insert(row[0],row[1],row[2],row[3],row[4])
    flightId = flights_insert(cur,row[5],row[6],row[7],row[8])
    #print(paxId, flightId, row[9])
    toInsert.append([paxId,flightId,row[9]])
fixMap = insertToDb(cur)
for row  in toInsert:
    if row[0] in fixMap:
        row[0] = fixMap[row[0]]
insertFlightsToDb(cur)
insertPaxFlights(cur,toInsert)
print(toInsert[0:20])
conn.commit()
conn.clonse()



