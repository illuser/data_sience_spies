


def selectFromBD(cur):

    tableInfo = []

    # TAB
    cur.execute('''SELECT PaxFirstName, PaxMiddleName, PaxLastName,PaxDocument, PaxAdditionalInfo3, Flight_code,
                     Departure_Airport, Arrival_Airport, DepartDate, Fare
                     FROM  Sirena_2''')
    
    tableInfo = cur.fetchall()
    

    # #CSV
    cur.execute('''SELECT PaxFirstName, PaxMiddleName, PaxLastName,PaxDocument, NULL, Flight_code,
                     NULL, Arrival_City, FlightDate, Fare
                     FROM  BoardingData_b_2''')

    tableInfo.extend(cur.fetchall())


    # JSON
    cur.execute('''SELECT PaxFirstName, NULL, PaxLastName,NULL, (Program || " " || Pass_code) CardNumber, Flight_code,
                     Departure_Airport, Arrival_Airport, Date, NULL
                     FROM  Privilege_name pn
                     join Privilege_Loyality_Programm pl on pl.Nickname=pn.Nickname
                    join Privilege_Registered_Flights pf on pf.Nickname=pn.Nickname''')

    tableInfo.extend(cur.fetchall())

    # YAML
    cur.execute('''SELECT NULL, NULL, NULL,NULL, (Program || " " || Pass_code) CardNumber, Flight_code,
                     Departure_Airport, Arrival_Airport, Date, Fare
                     FROM  Air_yaml''')

    tableInfo.extend(cur.fetchall())

    # XML
    cur.execute('''SELECT PaxFirstName, NULL, PaxLastName,NULL, (pzc.CardProgram || " " || pzc.CardNumber) CardNumber, Flight_code,
                     Departure_Airport, Arrival_Airport, FlightDate, Fare
                     FROM Pointz_user_2 pzu
                     join Pointz_card pzc on pzu.Uid=pzc.Uid
                     join Pointz_flight pzf on pzc.CardProgram=pzf.CardProgram and pzc.CardNumber=pzf.CardNumber''')

    tableInfo.extend(cur.fetchall())

    

    

    # XLSX
    cur.execute('''SELECT PaxFirstName, PaxMiddleName, PaxLastName,NULL, NULL, Flight_code,
                     Departure_City, Arrival_City, FlightDate, NULL
                     FROM  xlsxflight''')

    tableInfo.extend(cur.fetchall())

    return tableInfo