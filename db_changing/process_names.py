from sys import stderr

from transliter import translitMID, translitMVD
import sqlite3
from difflib import SequenceMatcher

xml_first_names = set()
xml_second_names = set()
xml_last_names = set()

xls_first_names = set()
xls_second_names = set()
xls_last_names = set()

tab_first_names = set()
tab_second_names = set()
tab_last_names = set()

json_first_names = set()
json_second_names = set()
json_last_names = set()

csv_first_names = set()
csv_second_names = set()
csv_last_names = set()


def fetch_csv():
    global csv_first_names, csv_last_names, csv_second_names

    conn = sqlite3.connect('./sqlite/BoardingData.csv.sqlite')
    c = conn.cursor()

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(passengerFirstName) from BoardingData_b_2''')

    for row in c.fetchall():
        csv_first_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(passengerSecondName) from BoardingData_b_2''')

    for row in c.fetchall():
        csv_second_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(passengerLastName) from BoardingData_b_2''')

    for row in c.fetchall():
        csv_last_names.add(row[0].replace('KS', 'X'))

    conn.close()


def fetch_tab():
    global tab_first_names, tab_last_names, tab_second_names

    conn = sqlite3.connect('./sqlite/Sirena-export-fixed.sqlite')
    c = conn.cursor()

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(paxFirstName) from Sirena''')

    for row in c.fetchall():
        tab_first_names.add(translitMVD(row[0]).upper())

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(paxSecondName) from Sirena''')

    for row in c.fetchall():
        tab_second_names.add(translitMVD(row[0]).upper())

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(paxLastName) from Sirena''')

    for row in c.fetchall():
        tab_last_names.add(translitMVD(row[0]).upper())

    conn.close()


def fetch_xml():
    global xml_first_names, xml_last_names, xml_second_names

    conn = sqlite3.connect('./sqlite/PointzAggregator-AirlinesData.sqlite')
    c = conn.cursor()

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(FirstName) from user_2''')

    for row in c.fetchall():
        xml_first_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(LastName) from user_2''')

    for row in c.fetchall():
        xml_last_names.add(row[0].replace('KS', 'X'))

    conn.close()


def fetch_json():
    global json_first_names, json_last_names, json_second_names

    conn = sqlite3.connect('./sqlite/FrequentFlyerForum-Profiles.json.db')
    c = conn.cursor()

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(First_Name) from Name''')

    for row in c.fetchall():
        if row[0] is not None:
            json_first_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(Last_Name) from Name''')

    for row in c.fetchall():
        if row[0] is not None:
            json_last_names.add(row[0].replace('KS', 'X'))

    conn.close()


def fetch_xls():
    global xls_first_names, xls_last_names, xls_second_names

    conn = sqlite3.connect('./sqlite/fromXLSX_v2b.db')
    c = conn.cursor()

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(First_Name) from xlsxflight_2''')

    for row in c.fetchall():
        if row[0] is not None:
            xls_first_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(Middle_Name) from xlsxflight_2''')

    for row in c.fetchall():
        if row[0] is not None:
            xls_second_names.add(row[0].replace('KS', 'X'))

    # noinspection SqlNoDataSourceInspection
    c.execute('''SELECT distinct(Last_Name) from xlsxflight_2''')

    for row in c.fetchall():
        if row[0] is not None:
            xls_last_names.add(row[0].replace('KS', 'X'))

    conn.close()


def correlate(sample, target, rank = 0.75):
    fail_count = 0
    search_count = 0
    count = 0

    for name in target:
        closest = ''
        dist = 0.0
        if name not in sample and name is not None:
            search_count += 1
            for match in sample:
                if match is None:
                    continue
                match_dist = SequenceMatcher(None, name.replace("'", ''), match.replace("'", '')).ratio()
                if match_dist > dist:
                    closest = match
                    dist = match_dist

            if closest == '' or dist < rank:
                fail_count += 1

            # print('%s -> %s: %f' % (name, closest, dist), file=stderr)
            if 0.0 < dist < rank:
                print('%s -> %s: %f' % (name, closest, dist), file=stderr)
        count += 1

    print('processed %d searched %d failed %d' % (count, search_count, fail_count))


if __name__ == '__main__':
    fetch_csv()
    fetch_tab()
    fetch_xml()
    fetch_xls()
    fetch_json()

    print('last names')

    last_name_set_list = [(csv_last_names, 'csv'),
                          (tab_last_names, 'tab'),
                          (xml_last_names, 'xml'),
                          (xls_last_names, 'xls'),
                          (json_last_names, 'json')]

    for i in last_name_set_list:
        for j in last_name_set_list:
            if i != j:
                print('correlate %5s <--- %5s' % (j[1], i[1]))
                correlate(j[0], i[0], 0.9)

    print('first names')

    first_name_set_list = [(csv_first_names, 'csv'),
                          (tab_first_names, 'tab'),
                          (xml_first_names, 'xml'),
                          (xls_first_names, 'xls'),
                          (json_first_names, 'json')]

    for i in first_name_set_list:
        for j in first_name_set_list:
            if i != j:
                print('correlate %5s <--- %5s' % (j[1], i[1]))
                correlate(j[0], i[0], 0.9)

    print('second names')

    second_name_set_list = [(csv_second_names, 'csv'),
                          (tab_second_names, 'tab'),
                          (xml_second_names, 'xml'),
                          (xls_second_names, 'xls'),
                          (json_second_names, 'json')]

    for i in second_name_set_list:
        for j in second_name_set_list:
            if i != j:
                print('correlate %5s <--- %5s' % (j[1], i[1]))
                correlate(j[0], i[0], 0.9)
    # print('correlate csv tab')
    # correlate(csv_last_names, tab_last_names)
    #
    # print('correlate csv xml')
    # correlate(csv_last_names, xml_last_names)
    #
    # print('correlate tab xml')
    # correlate(tab_last_names, xml_last_names)
