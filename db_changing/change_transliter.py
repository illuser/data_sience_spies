from transliterate import translit, get_available_language_codes
from transliterate.base import TranslitLanguagePack, registry
import sqlite3

class MidLanguagePack(TranslitLanguagePack):
    language_code = "ru_mid"
    language_name = "Russian MID"
    reversed_specific_mapping = (
        u"йЙёэЁЭъьЪЬ",
        u"iIeeEE''''"
    )
    mapping = (
        u"abvgdeziklmnoprstufhcC'y'ABVGDEZIKLMNOPRSTUFH'Y'",
        u"абвгдезиклмнопрстуфхцЦъыьАБВГДЕЗИКЛМНОПРСТУФХЪЫЬ",
    )

    pre_processor_mapping = {
        u"kh": u"x",
        u"Kh": u"Х",
        u"zh": u"ж",
        u"ts": u"ц",
        u"ch": u"ч",
        u"sh": u"ш",
        u"shch": u"щ",
        u"iu": u"ю",
        u"ia": u"я",
        u"Zh": u"Ж",
        u"Ts": u"Ц",
        u"Ch": u"Ч",
        u"Sh": u"Ш",
        u"Shch": u"Щ",
        u"Iu": u"Ю",
        u"Ia": u"Я",
        u"Ii": u"ИЙ"
    }

class GostLanguagePack(TranslitLanguagePack):
    language_code = "ru_gost"
    language_name = "Russian GOST"
    reversed_specific_mapping = (
        u"йЙёэЁЭъьЪЬ",
        u"iIeeEE''''"
    )
    mapping = (
        u"abvgdeziklmnoprstufhcC'y'ABVGDEZIKLMNOPRSTUFH'Y'",
        u"абвгдезиклмнопрстуфхцЦъыьАБВГДЕЗИКЛМНОПРСТУФХЪЫЬ",
    )

    pre_processor_mapping = {
        u"kh": u"x",
        u"Kh": u"Х",
        u"zh": u"ж",
        u"ts": u"ц",
        u"ch": u"ч",
        u"sh": u"ш",
        u"shch": u"щ",
        u"iu": u"ю",
        u"ia": u"я",
        u"Zh": u"Ж",
        u"Tc": u"Ц",
        u"Ch": u"Ч",
        u"Sh": u"Ш",
        u"Shch": u"Щ",
        u"Iu": u"Ю",
        u"Ia": u"Я",
        u"Ii": u"ИЙ"
    }

class MvdLanguagePack(TranslitLanguagePack):
    language_code = "ru_mvd"
    language_name = "Russian MVD"
    reversed_specific_mapping = (
        u"ёэЁЭъьЪЬ",
        u"eeEE''''"
    )
    mapping = (
        u"abvgdeziklmnoprstufhcC'y'ABVGDEZIKLMNOPRSTUFH'Y'",
        u"абвгдезиклмнопрстуфхцЦъыьАБВГДЕЗИКЛМНОПРСТУФХЪЫЬ",
    )

    pre_processor_mapping = {
        u"Y": u"Й",
        u"y": u"й",
        u"kh": u"x",
        u"Kh": u"Х",
        u"zh": u"ж",
        u"ts": u"ц",
        u"ch": u"ч",
        u"sh": u"ш",
        u"shch": u"щ",
        u"yu": u"ю",
        u"ya": u"я",
        u"Zh": u"Ж",
        u"Ts": u"Ц",
        u"Ch": u"Ч",
        u"Sh": u"Ш",
        u"Shch": u"Щ",
        u"Yu": u"Ю",
        u"Ya": u"Я",
        u"Iy": u"ИЙ"
    }


registry.register(MidLanguagePack)
registry.register(MvdLanguagePack)
registry.register(GostLanguagePack)

conn = sqlite3.connect("C:/Users/illuser/Documents/Data Sience Addit/DBs/Sirena-export-fixed-15_24.sqlite")
cursor = conn.cursor()

surnames = {}
names = {}

cursor.execute("select distinct paxLastName from Sirena")
tmp=cursor.fetchall()
for i in tmp:
    # mid to mvd with 'X' instead 'KS'
    surnames[translit(i[0], 'ru_mid', reversed=True).upper()] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    # just match mvd strings with 'KS' to 'X'
    surnames[translit(i[0], 'ru_mvd', reversed=True).upper()] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    # gost (only 'ц'='tc') without \' to mvd
    surnames[translit(i[0], 'ru_gost', reversed=True).upper().replace('\'', '')] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    # mid without \' to mvd
    surnames[translit(i[0], 'ru_mid', reversed=True).upper().replace('\'', '')] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')

cursor.execute("select distinct paxFirstName from Sirena")
tmp=cursor.fetchall()
for i in tmp:
    names[translit(i[0], 'ru_mid', reversed=True).upper()] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    names[translit(i[0], 'ru_mvd', reversed=True).upper()] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    names[translit(i[0], 'ru_gost', reversed=True).upper().replace('\'', '')] = \
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')
    names[translit(i[0], 'ru_mid', reversed=True).upper().replace('\'', '')] =\
        translit(i[0], 'ru_mvd', reversed=True).upper().replace('KS', 'X')

print(len(surnames))
print(len(names))

conn = sqlite3.connect("C:/Users/illuser/Documents/Data Sience Addit/DBs/fromXLSX_v2.db")
cursor = conn.cursor()

cursor.execute("select * from xlsxflight")
tmp=cursor.fetchall()
#cursor.execute("CREATE TABLE xlsxflight_2 ( 'First_Name' text, 'Middle_Name' text, 'Last_Name' text, 'Sex' text,"
#               " 'Flight' text, 'CityFromAbbr' text, 'CityToAbbr' text, 'Date' text, 'Time' text, 'CityFromFull' text,"
#               " 'CityToFull' text, 'PassClass' text, 'SequenceNumber' text, 'ETicket' text, 'PNR' text)")


mistake = 0
mist_names = []
mist_surnames = []
for row in tmp:
    new_row = list(row)
    try:
        new_row[0] = names[row[0]]
        new_row[2] = surnames[row[2]]
        cursor.execute("INSERT INTO xlsxflight_2 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", new_row)
    except:
        if row[0] not in names.values() or row[2] not in surnames.values():
            mistake += 1
            mist_names.append(row[0])
            mist_surnames.append(row[2])


print("Mistakes: ", mistake)
if mistake == 0:
    conn.commit()
#print(mist_names[0])
#print(mist_surnames[0])





