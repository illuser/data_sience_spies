#### YAML
It contents 2 scripts: 
- the first one divide `yaml_src` into 
20 parts (1 common file is too hard for our systems);
- the second one converts yaml to `sqlite-db` format

#### XML
`xml_to_sqlite.py` converts xml to `sqlite-db` format.

#### JSON
It contents 3 scripts. Everyone converts part of data in apropriate 
db of `sqlite-format`.

#### XLSX
The script `full_xlsx_proc.py` converts `xlsx` files to `sqlite-db` format and
adduces the transliterations to 1 commont type, using `xml` and `json` dbs.

#### TAB
`tab_to_sqlite.py` converts `tab` to `sqlite-db` format.