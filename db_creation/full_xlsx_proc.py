# we take info from sheets of xlsx files, process it and put to the DB. json and xml DB are needed for name dictionary.

import xlrd
import sqlite3
import re
from os import listdir

#collecting names dictionary, so other word will be surname
conn = sqlite3.connect("/Users/kirkondrash/Desktop/XML.db")
cursor = conn.cursor()

cursor.execute("select distinct firstName from user")
tmp=cursor.fetchall()
namesL=[]
for i in tmp:
	namesL.append(i[0])

conn = sqlite3.connect("/Users/kirkondrash/Desktop/json.db")
cursor = conn.cursor()

cursor.execute("select first_name from name")
tmp=cursor.fetchall()
for i in tmp:
	namesL.append(i[0])

names=set(namesL)
#making a DB from xlsx files
conn = sqlite3.connect("/Users/kirkondrash/Desktop/fromXLSX_v2.db")
cursor = conn.cursor()

path='/Users/kirkondrash/Desktop/drive-download-20181119T203356Z-001/YourBoardingPassDotAero/'

cursor.execute("CREATE TABLE xlsxflight ( 'First_Name' text, 'Middle_Name' text, 'Last_Name' text, 'Sex' text, 'Flight' text, 'CityFromAbbr' text, 'CityToAbbr' text, 'Date' text, 'Time' text, 'CityFromFull' text, 'CityToFull' text, 'PassClass' text, 'SequenceNumber' text, 'ETicket' text, 'PNR' text)")

for f in listdir(path):
	wb=xlrd.open_workbook(path+f,on_demand = True) #open excel file
	for sh in wb.sheets(): #work with each sheet
		queryvars=["", "", "", sh.cell(2,0).value, sh.cell(4,0).value, sh.cell(6,3).value, sh.cell(6,7).value, sh.cell(8,0).value, sh.cell(8,2).value, sh.cell(4,3).value, sh.cell(4,7).value, sh.cell(2,7).value, sh.cell(0,7).value, sh.cell(12,4).value, sh.cell(12,1).value] #all values except null, unnecessary/not interesting; three first cells are for names
		tmpname=sh.cell(2,1).value #we wirk with name 'cause it is really, really bad in the original. Don't ever do it like this
		tmp=re.split(' ', tmpname) #two/three words
        	tmp.sort(key = lambda s: len(s)) #we have middle_name only of one letter, so it'll be on first place
        	if len(tmp)==3:
                	queryvars[1]=tmp[0] #middle_name, if present
                	tmp.pop(0)
        	if tmp[0] in names: #we have two words, one of which is name, other surname
                	queryvars[0]=tmp[0]
                	queryvars[2]=tmp[1]
        	elif tmp[1] in names:
                	queryvars[0]=tmp[1]
                	queryvars[2]=tmp[0]
        	else:
                	print ("NoNameException: "+ tmp[0]+" "+tmp[1]) #who knows?
		cursor.execute("INSERT INTO xlsxflight VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",queryvars) #fill the DB with our info
	wb.release_resources()
cursor.execute("update xlsxflight set Middle_Name=null where Middle_Name=''")
conn.commit()



