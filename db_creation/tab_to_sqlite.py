import sqlite3
from ordered_set import OrderedSet

lines = []

conn = sqlite3.connect('../Airlines/Sirena-export-fixed.sqlite')
c = conn.cursor()

fullname_length = 60
# ***************birthdat************************code****************fare
field_lengths = (12, 12, 12, 12, 12, 6, 6, 6, 6, 6, 18, 12, 6, 6, 6, 6, 12, 19, 5, 36, 60)  # first field processed separately

sid = 0
# noinspection SqlNoDataSourceInspection
c.execute('''DROP TABLE IF EXISTS Sirena''')
# noinspection SqlNoDataSourceInspection
c.execute('''CREATE TABLE Sirena
             (sid int PRIMARY KEY, paxLastName text, paxFirstName text, paxSecondName text, PaxBirthDate text, 
              DepartDate text, DepartTime text, ArrivalDate text, ArrivalTime text, FlightCode text, Sh text, FromAF text, 
              DestAF text, Code text, eTicket text, TravelDoc text, Seat text, Meal text, TrvCls text, Fare text,  
              Baggage text, PaxAdditionalInfo1 text, PaxAdditionalInfo2 text, PaxAdditionalInfo3 text, AgentInfo text)
              ''')

with open('../Airlines/Sirena/Sirena-export-fixed-fixed.tab', 'rt') as f:
    for line in f:
        line_content = [sid]
        fullname = line[0:fullname_length]
        split_name = fullname.strip().split(' ')
        assert len(split_name) == 3
        line_content += split_name

        pos = fullname_length
        for length in field_lengths:
            raw_line = line[pos:pos + length].strip().replace('FF#', '')
            if raw_line == '' or raw_line == 'N/A':
                raw_line = None
            line_content.append(raw_line)
            pos += length

        sid += 1
        lines.append(line_content)
        # print(line_content)

# noinspection SqlNoDataSourceInspection
c.executemany('INSERT INTO Sirena VALUES(?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', lines)

conn.commit()
conn.close()
