import xml.etree.ElementTree as etree
import sqlite3
from ordered_set import OrderedSet

uidSet = set()
cardNumSet = set()
flightCodeSet = set()

userSet = OrderedSet()
cardSet = OrderedSet()
flightSet = OrderedSet()

conn = sqlite3.connect('../Airlines/PointzAggregator-AirlinesData.sqlite')
c = conn.cursor()

# noinspection SqlNoDataSourceInspection
c.execute('''DROP TABLE  IF EXISTS user''')
# noinspection SqlNoDataSourceInspection
c.execute('''CREATE TABLE user
             (uid int PRIMARY KEY, firstName text, lastName test)''')

# noinspection SqlNoDataSourceInspection
c.execute('''DROP TABLE  IF EXISTS card''')
# noinspection SqlNoDataSourceInspection
c.execute('''CREATE TABLE card
             (cardProgram text, cardNumber text, bonusProgram text, uid int,
              PRIMARY KEY (cardProgram, cardNumber), 
              FOREIGN KEY(uid) REFERENCES user(uid))''')

# noinspection SqlNoDataSourceInspection
c.execute('''DROP TABLE  IF EXISTS flight''')
# noinspection SqlNoDataSourceInspection
c.execute('''CREATE TABLE flight
             (flightId int, code text , flightDate date, departure text, arrival text, fare text, cardProgram text, 
             cardNumber text, FOREIGN KEY (cardProgram, cardNumber) REFERENCES card(cardProgram, cardNumber))''')

flightId = 0

tree = etree.parse('../Airlines/PointzAggregator-AirlinesData.xml')
root = tree.getroot()
for user in root:
    uid = user.get('uid')
    firstName = user.find('name').get('first')
    lastName = user.find('name').get('last')
    # print('User: \tuid %d; user %s %s' % (int(uid), firstName, lastName))

    if uid in uidSet:
        print('User: \tuid %d; user %s %s NOT UNIQUE' % (int(uid), firstName, lastName))
    else:
        uidSet.add(uid)
        userSet.add((uid, firstName, lastName))

    cards = user.find('cards')
    for card in cards:
        cardNumber = card.get('number')
        bonusProgram = card.find('bonusprogramm').text
        # print('Card: \tnumber %s bonus programm %s' % (cardNumber, bonusProgram))

        if cardNumber in cardNumSet:
            print('Card: \tnumber %s bonus programm %s NOT UNIQUE' % (cardNumber, bonusProgram))
        else:
            cardNumSet.add(cardNumber)
            cardProg = cardNumber.strip().split(' ')[0]
            cardNum = cardNumber.strip().split(' ')[1]
            cardSet.add((cardProg, cardNum, bonusProgram, uid))

        activities = card.find('activities')
        for activity in activities:
            code = activity.find('Code').text
            date = activity.find('Date').text
            departure = activity.find('Departure').text
            arrival = activity.find('Arrival').text
            fare = activity.find('Fare').text
            # print('Flight: \tcode %s date %s dep %s arr %s dare %s' % (code, date, departure, arrival, fare))

            if (code, date) in flightCodeSet:
                print('Flight: \tcode %s date %s dep %s arr %s dare %s NOT UNIQUE' % (code, date, departure, arrival, fare))
            else:
                flightCodeSet.add((code, date))
                cardProg = cardNumber.strip().split(' ')[0]
                cardNum = cardNumber.strip().split(' ')[1]
                flightSet.add((flightId, code, date, departure, arrival, fare, cardProg, cardNum))
                flightId += 1

print('user %d = 66657' % len(userSet))
print('card %d = 88818' % len(cardSet))
print('flight %d = 436090' % len(flightSet))

# noinspection SqlNoDataSourceInspection
c.executemany('INSERT INTO user VALUES(?,?,?)', userSet)


# noinspection SqlNoDataSourceInspection
c.executemany('INSERT INTO card VALUES(?,?,?,?)', cardSet)

# noinspection SqlNoDataSourceInspection
c.executemany('INSERT INTO flight VALUES(?,?,?,?,?,?,?,?)', flightSet)

conn.commit()
conn.close()
