import json
import sqlite3

conn = sqlite3.connect("Name.db")
cursor = conn.cursor()
cursor.execute("""CREATE TABLE Registered_Flights
                  (Nickname text, Date data, Codeshare text, Arrival_City text,
                  Arrival_Airport text, Arrival_Country text, Flight text,
                  Departure_City text, Departure_Airport text, Departure_Country text)
               """)
data = []
row = ()
Forum_Profiles = json.load(open('FrequentFlyerForum-Profiles.json'))
for i in Forum_Profiles["Forum Profiles"]:
    for j in i["Registered Flights"]:
        row = (i["NickName"], j["Date"], j["Codeshare"], j["Arrival"]["City"],
               j["Arrival"]["Airport"],j["Arrival"]["Country"], j["Flight"],
               j["Departure"]["City"], j["Departure"]["Airport"], j["Departure"]["Country"])
    # print(i["NickName"])
    # print(i["Sex"])
    # print(i["Real Name"]["Last Name"])
    # print(i["Real Name"]["First Name"])
    # for j in i["Registered Flights"]:
    #     print(j)
    # print(row)
        data.append(row)
cursor.executemany("INSERT INTO Registered_Flights VALUES (?,?,?,?,?,?,?,?,?,?)", data)
conn.commit()
