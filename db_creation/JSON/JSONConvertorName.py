import json
import sqlite3

conn = sqlite3.connect("Name.db")
cursor = conn.cursor()
cursor.execute("""CREATE TABLE Name
                  (Nickname text, Sex text, Last_name text,
                   First_name text)
               """)

data = []
row = ()
Forum_Profiles = json.load(open('FrequentFlyerForum-Profiles.json'))
for i in Forum_Profiles["Forum Profiles"]:
    row = (i["NickName"], i["Sex"], i["Real Name"]["Last Name"], i["Real Name"]["First Name"])
    # print(i["NickName"])
    # print(i["Sex"])
    # print(i["Real Name"]["Last Name"])
    # print(i["Real Name"]["First Name"])
    # for j in i["Registered Flights"]:
    #     print(j)
    # print(row)
    data.append(row)
cursor.executemany("INSERT INTO Name VALUES (?,?,?,?)", data)
conn.commit()
