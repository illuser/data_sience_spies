# Data_Sience_Spies

We use `Python` with `sqlite` for files analyzing.

#### db_creation
This directory contents all files, that were used to create `.sqlite` or `.db`
files from source files.

#### db_changing
This directory contents files, that changed dbs in different ways and led them
in 'comfortable form'.

#### DBs_scheme
It is a scheme of all our databases.